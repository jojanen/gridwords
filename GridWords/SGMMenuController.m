//
//  SGViewController.m
//  Scramble
//
//  Created by John Ojanen on 3/2/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import "SGMMenuController.h"
#import "SGGameViewController.h"
#import "SGSettingsKeys.h"
#import "SGSettingsViewController.h"

#import "SGNumberPuzzle.h"
#import "SGColorPuzzle.h"
#import "SGWordPuzzle.h"

#import "PureLayout.h"
#import "UIFont+SystemOverride.h"

@interface SGMMenuController ()

@property SGPuzzle *activePuzzle;
@property (weak, nonatomic) IBOutlet UILabel *gridSizeLabel;
@property (weak, nonatomic) IBOutlet UIButton *noneButton;
@property (weak, nonatomic) IBOutlet UIButton *threeMinButton;
@property (weak, nonatomic) IBOutlet UIButton *fiveMinButton;

- (IBAction)minusButton:(id)sender;
- (IBAction)plusButton:(id)sender;
- (IBAction)handleTimeButtons:(id)sender;
@end

@implementation SGMMenuController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	self.gridSizeLabel.text = [NSString stringWithFormat:@"%ld",(long)[userDefaults integerForKey:kGridSizeX]];
	
	NSInteger gameTime = [userDefaults integerForKey:kGameTime];
	if (gameTime == 0)
		self.noneButton.selected = YES;
	else if (gameTime == 180)
		self.threeMinButton.selected = YES;
	else
		self.fiveMinButton.selected = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)playGame:(id)sender
{
	SGGameViewController *gameViewController  = [[SGGameViewController alloc] initWithNibName:@"SGGameViewController_iPhone" bundle:nil];

	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSInteger rows = [userDefaults integerForKey:kGridSizeX];
	NSInteger columns = [userDefaults integerForKey:kGridSizeX];
	NSInteger startingLevel = [userDefaults integerForKey:kPuzzleOneLevel];
	self.activePuzzle = [[SGWordPuzzle alloc] initWithRows:rows andColumns:columns];
	
	NSTimeInterval gameTime = [userDefaults integerForKey:kGameTime];
	
	[self.navigationController pushViewController:gameViewController animated:YES];
	[gameViewController setPuzzle:self.activePuzzle level:startingLevel timeAllowed:gameTime];
}


- (IBAction)playColorsPuzzile:(id)sender
{
	SGGameViewController *gameViewController = [[SGGameViewController alloc] initWithNibName:@"SGGameViewController_iPhone" bundle:nil];
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSInteger rows = 5;	// [userDefaults integerForKey:kGridSizeX];
	NSInteger columns = 5; // [userDefaults integerForKey:kGridSizeX];
	NSInteger startingLevel = [userDefaults integerForKey:kPuzzleOneLevel];
	self.activePuzzle = [[SGWordPuzzle alloc] initWithRows:rows andColumns:columns];
	
	NSTimeInterval gameTime = [userDefaults integerForKey:kGameTime];
	[gameViewController setPuzzle:self.activePuzzle level:startingLevel timeAllowed:gameTime];
	
	[self.navigationController pushViewController:gameViewController animated:YES];
}


- (IBAction)showSettings:(id)sender
{
	SGSettingsViewController *settingViewController = [[SGSettingsViewController alloc] initWithNibName:@"SGSettingsViewController_iPhone" bundle:nil];

	[self.navigationController pushViewController:settingViewController animated:YES];
}


- (IBAction)minusButton:(id)sender
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSInteger gridSize = [userDefaults integerForKey:kGridSizeX];
	gridSize -= 1;
	[userDefaults setInteger:gridSize forKey:kGridSizeX];
	self.gridSizeLabel.text = [NSString stringWithFormat:@"%ld",(long)gridSize];
}


- (IBAction)plusButton:(id)sender
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSInteger gridSize = [userDefaults integerForKey:kGridSizeX];
	gridSize += 1;
	[userDefaults setInteger:gridSize forKey:kGridSizeX];
	self.gridSizeLabel.text = [NSString stringWithFormat:@"%ld",(long)gridSize];
}


- (IBAction)handleTimeButtons:(id)sender
{
	UIButton *tappedButton = (UIButton *)sender;
	
	self.noneButton.selected = NO;
	self.threeMinButton.selected = NO;
	self.fiveMinButton.selected = NO;
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSInteger gameTime = [userDefaults integerForKey:kGameTime];
	
	NSString *buttonTitle = tappedButton.titleLabel.text;
	if ([buttonTitle isEqualToString:@"None"])
	{
		self.noneButton.selected = YES;
		gameTime = 0;
	}
	else if ([buttonTitle isEqualToString:@"3 Min"])
	{
		self.threeMinButton.selected = YES;
		gameTime = 180;
	}
	else
	{
		self.fiveMinButton.selected = YES;
		gameTime = 500;
	}
	[userDefaults setInteger:gameTime forKey:kGameTime];

}


@end
