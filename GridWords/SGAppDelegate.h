//
//  SGAppDelegate.h
//  Scramble
//
//  Created by John Ojanen on 3/2/18.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SGMMenuController;

@interface SGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SGMMenuController *viewController;
@property (strong, nonatomic) UINavigationController *navController;

@end
