//
//  SGGridPosition.h
//  Scramble
//
//  Created by John Ojanen on 10/13/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef struct {
	NSInteger row;
	NSInteger column;
} SGGridPosition;

static inline SGGridPosition SGGridPositionMake(NSInteger row, NSInteger column);

static inline SGGridPosition SGGridPositionMake(NSInteger row, NSInteger column)
{
	return (SGGridPosition) {row, column};
}

//@interface SGGridPosition : NSObject
//{
//}
//@property (nonatomic, assign) NSUInteger row;
//@property (nonatomic, assign) NSUInteger col;
//
//- (instancetype)initWithRow:(NSUInteger)row andColumn:(NSUInteger)column;
//
//+ (SGGridPosition *)positionForRow:(NSUInteger)row col:(NSUInteger)col;
//
//- (NSUInteger)incrementRow;
//- (NSUInteger)decrementRow;
//- (NSUInteger)incrementCol;
//- (NSUInteger)decrementCol;
//
//@end


