//
//  SGGameViewController.h
//  Scramble
//
//  Created by John Ojanen on 4/14/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SGPuzzle;

@interface SGGameViewController : UIViewController

- (IBAction)donePressed:(id)sender;
- (IBAction)showSettings:(id)sender;
- (IBAction)checkPressed:(id)sender;

- (void)setPuzzle:(SGPuzzle *)inPuzzle level:(NSInteger)inLevel timeAllowed:(NSTimeInterval)time;
- (NSInteger)lastLevelPlayed;

@end
