//
//  SGGridPosition.m
//  Scramble
//
//  Created by John Ojanen on 10/13/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGGridPosition.h"



//@implementation SGGridPosition
//
////_______________________________________________________________________________________________________________
////
//
//
//- (instancetype)initWithRow:(NSUInteger)row andColumn:(NSUInteger)column
//{
//	self = [super init];
//	if (self != nil)
//	{
//		self.row = row;
//		self.col = column;
//	}
//	
//	return self;
//}
//
//
////_______________________________________________________________________________________________________________
////
//
//+ (SGGridPosition *)positionForRow:(NSUInteger)row col:(NSUInteger)col
//{
//	SGGridPosition *pos = [[SGGridPosition alloc] initWithRow:row andColumn:col];
//	return pos;
//}
//
//
////_______________________________________________________________________________________________________________
////
//
//- (NSUInteger)incrementRow
//{
//	self.row += 1;
//	return self.row;
//}
//
//
////_______________________________________________________________________________________________________________
////
//
//- (NSUInteger)decrementRow
//{
//	if (self.row > 0)
//		self.row -= 1;
//	return self.row;
//}
//
//
////_______________________________________________________________________________________________________________
////
//
//- (NSUInteger)incrementCol
//{
//	self.col += 1;
//	return self.col;
//}
//
//
////_______________________________________________________________________________________________________________
////
//
//- (NSUInteger)decrementCol
//{
//	if (self.col > 0)
//		self.col -= 1;
//	return self.col;
//}
//
//
//@end
