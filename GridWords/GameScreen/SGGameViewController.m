//
//  SGGameViewController.m
//  Scramble
//
//  Created by John Ojanen on 4/14/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import "SGGameViewController.h"
#import "UIFont+SystemOverride.h"
#import "SGFinishedPuzzleViewController.h"
//#import "SGFoundWordsView.h"
#import "SGGridView.h"
#import "SGWordPuzzle.h"
#import "SGSettingsKeys.h"
#import "SGLabelTileView.h"
#import "SGSettingsViewController.h"
#import "PureLayout.h"

#import "GridWords-Swift.h"

@import AVFoundation;



//_______________________________________________________________________________________________________________
//

@interface SGGameViewController () <SGGridViewDataSource, SGGridViewDelegate, SGFinishedScreenDelegate>
{
	NSInteger _cachedRows;
	NSInteger _cachedColumns;
	NSInteger _score;
	NSTimeInterval _solveTime;
	BOOL _scrollViewSetupDone;
	NSTimeInterval _gameTimeInterval;
}

@property (nonatomic) SGGridView *puzzleView;
@property (nonatomic) UIScrollView *scroller;
@property (nonatomic) SGPuzzle *currPuzzle;
@property (nonatomic) SGFoundWordsView *foundWordsView;
@property (nonatomic) NSTimer *puzzleTimer;
@property (nonatomic) NSDateFormatter *timerFormatter;
@property (nonatomic) NSDate *endTime;
@property (nonatomic) NSInteger puzzleLevel;
@property (nonatomic) UITapGestureRecognizer *dismissFoundWordsTapGR;
@property (nonatomic) UITapGestureRecognizer *checkForWordsTapGR;

@property (nonatomic) NSMutableArray *puzzleCells;
@property (nonatomic) NSMutableSet *usedWords;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *puzzleTimeLabel;

//@property (nonatomic, strong) AEAudioController *audioController;
@property (nonatomic) AVAudioPlayer *clickSound;
@property (nonatomic) AVAudioPlayer *wordFoundSound;
@property (nonatomic) AVAudioPlayer *noWordSound;

- (IBAction)showFoundWords:(id)sender;
- (IBAction)hideFoundWords:(id)sender;
- (void)foundWordsViewTapped;

- (void)timerFireMethod:(NSTimer *)timer;

- (void)setPuzzleData;
-(NSMutableDictionary *)cellDictionaryAtPosition:(SGGridPosition)position;

- (UIImage *)imageFromViewWithScale:(UIView *)inView scale:(CGFloat)scale;

- (NSArray *)getChangedWords:(BOOL)movedVertically indexMoved:(NSInteger)rowOrColumnIndex;
- (NSString *)wordForRow:(NSInteger)row;
- (NSString *)wordForColumn:(NSInteger)col;

@end


//_______________________________________________________________________________________________________________
//


@implementation SGGameViewController


static const CGFloat kTopMargin = 55.0;
static const CGFloat kBottomMargin = 100.0;
static const CGFloat kLeftMargin = 25.0;
static const CGFloat kRightMargin = 25.0;
//static const NSTimeInterval kGameTimeInterval = 60.0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//_______________________________________________________________________________________________________________
//

-(void)dealloc
{
}


//_______________________________________________________________________________________________________________
//

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	// Add the grid view
	self.puzzleView = [[SGGridView alloc] initWithFrame:self.view.frame];
	self.puzzleView.dataSource = self;
	self.puzzleView.delegate = self;
	[self.view addSubview:self.puzzleView];
	ALEdgeInsets insets;
	insets.top = kTopMargin;
	insets.left = kLeftMargin;
	insets.bottom = kBottomMargin;
	insets.right = kRightMargin;
	[self.puzzleView autoPinEdgesToSuperviewEdgesWithInsets:insets];

	// Make and add the hidden scrollview that controls decelleration.
	self.scroller = [[UIScrollView alloc] init];
	self.scroller.showsHorizontalScrollIndicator = NO;
	self.scroller.decelerationRate = UIScrollViewDecelerationRateFast;
	self.scroller.directionalLockEnabled = YES;
	self.scroller.delegate = self.puzzleView;
	self.scroller.hidden = YES;
	[self.view addSubview:self.scroller];

	[self.puzzleView addGestureRecognizer:self.scroller.panGestureRecognizer];
	
//	_gameTimeInterval = kGameTimeInterval;
//	self.endTime = [NSDate dateWithTimeIntervalSinceNow:_gameTimeInterval];
	
	// Create a date formatter
	self.timerFormatter = [[NSDateFormatter alloc] init];
	[self.timerFormatter setDateFormat:@"mm:ss"];
	
	
	// Load sounds here.
	NSURL *clickfile = [[NSBundle mainBundle] URLForResource:@"scrollClick" withExtension:@"mp3"];
	self.clickSound = [[AVAudioPlayer alloc] initWithContentsOfURL:clickfile error:NULL];
	[self.clickSound setVolume:1.0];
	
	NSURL *foundfile = [[NSBundle mainBundle] URLForResource:@"wordFound" withExtension:@"mp3"];
	self.wordFoundSound = [[AVAudioPlayer alloc] initWithContentsOfURL:foundfile error:NULL];
	[self.wordFoundSound setVolume:1.0];

	NSURL *notfoundfile = [[NSBundle mainBundle] URLForResource:@"wordNotFound" withExtension:@"mp3"];
	self.noWordSound = [[AVAudioPlayer alloc] initWithContentsOfURL:notfoundfile error:NULL];
	[self.noWordSound setVolume:1.0];

	
	self.usedWords = [[NSMutableSet alloc] init];
	self.puzzleLevel = 1;
	_scrollViewSetupDone = NO;
	self.puzzleView.hidden = YES;
	
}


//_______________________________________________________________________________________________________________
//

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	// Kinda cheesy but the behind the scenes scroll view needs to know the cell
	// size to set the offset correctly. This can only be calculated after auto layout
	// has occured. Is there a better way?
	if (!_scrollViewSetupDone)
	{
		self.scroller.frame = self.puzzleView.frame;
		CGFloat cellWidth = self.puzzleView.bounds.size.width / _cachedColumns;
		CGFloat cellHeight = self.puzzleView.bounds.size.height / _cachedRows;
		CGRect scrollRect = CGRectInset(self.puzzleView.frame, -(100 * cellWidth), -(100 * cellHeight));
		self.scroller.contentSize = scrollRect.size;
		self.scroller.contentOffset = CGPointMake(cellWidth * 48, cellHeight * 48);
		_scrollViewSetupDone = YES;
	}
}


//_______________________________________________________________________________________________________________
//

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	self.puzzleView.hidden = NO;
	[self.puzzleView reloadData];
}


//_______________________________________________________________________________________________________________
//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//_______________________________________________________________________________________________________________
//

- (void)setPuzzle:(SGPuzzle *)inPuzzle level:(NSInteger)inLevel timeAllowed:(NSTimeInterval)time
{
	self.puzzleLevel = inLevel;
	self.currPuzzle = inPuzzle;
	_cachedRows = self.currPuzzle.rowCount;
	_cachedColumns = self.currPuzzle.colCount;
	
	_gameTimeInterval = time;

	[self.currPuzzle generateLevel:self.puzzleLevel];			// generate a puzzle
	
	[self createFoundWordsView];			// save off an image of the solved version of puzzle
	[self.currPuzzle scramble];				// mix up puzzle cells
	[self setPuzzleData];					// save puzzle data to our dictionary of cells.
	[self startPuzzle];
}


//_______________________________________________________________________________________________________________
//

- (void)createFoundWordsView
{
	self.foundWordsView = [[SGFoundWordsView alloc] initWithFrame:self.view.frame];
	self.foundWordsView.hidden = YES;
	[self.view addSubview:self.foundWordsView];

	ALEdgeInsets insets;
	insets.top = 0.0;
	insets.left = 0.0;
	insets.bottom = 0.0;
	insets.right = 0.0;
	[self.foundWordsView autoPinEdgesToSuperviewEdgesWithInsets:insets];
	
	self.dismissFoundWordsTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundWordsViewTapped)];
	[self.foundWordsView addGestureRecognizer:self.dismissFoundWordsTapGR];

	self.checkForWordsTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(wordGridViewDoubleTapped)];
	self.checkForWordsTapGR.numberOfTapsRequired = 2;
	[self.puzzleView addGestureRecognizer:self.checkForWordsTapGR];
}


//_______________________________________________________________________________________________________________
//
//_______________________________________________________________________________________________________________
//

- (void)setPuzzleData
{
	_cachedRows = self.currPuzzle.rowCount;
	_cachedColumns = self.currPuzzle.colCount;
	
	self.puzzleCells = [[NSMutableArray alloc] initWithCapacity:(_cachedRows * _cachedColumns)];
	for(int row = 0; row < _cachedRows; row++)
	{
		for(int col = 0; col < _cachedColumns; col++)
		{
			SGTileView *cell = [self.currPuzzle createTileForX:col Y:row];
			
			NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
			dict[@"Cell"] = cell;
			dict[@"Row"] = @(row);
			dict[@"Column"] = @(col);
			
			[self.puzzleCells addObject:dict];
		}
	}
	
}

//_______________________________________________________________________________________________________________
//

- (IBAction)donePressed:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}


//_______________________________________________________________________________________________________________
//

- (IBAction)checkPressed:(id)sender
{
	[self checkPuzzle];
}


//_______________________________________________________________________________________________________________
//

- (IBAction)showSettings:(id)sender
{
	SGSettingsViewController *settingViewController = [[SGSettingsViewController alloc] initWithNibName:@"SGSettingsViewController_iPhone" bundle:nil];
//	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//		settingViewController = [[SGSettingsViewController alloc] initWithNibName:@"SGSettingsViewController_iPhone" bundle:nil];
//	} else {
//	    settingViewController = [[SGSettingsViewController alloc] initWithNibName:@"SGSettingsViewController_iPad" bundle:nil];
//	}
	
	[self.navigationController pushViewController:settingViewController animated:YES];
}


//_______________________________________________________________________________________________________________
// SGGridViewDataSource callbacks

- (NSInteger)numberOfRowsInGridView:(SGGridView *)gridView
{
	return _cachedRows;
}


//_______________________________________________________________________________________________________________
//

- (NSInteger)numberOfColumnsInGridView:(SGGridView *)gridView
{
	return _cachedColumns;
}


//_______________________________________________________________________________________________________________
//

-( SGLabelTileView *)gridView:(SGGridView *)gridView tileForPosition:(SGGridPosition)position;
{
	NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:position];
	SGLabelTileView *cell = cellDict[@"Cell"];
	
	if (!cell)
	{
		position = [self normalizePosition:position inGridView:self.puzzleView];
		cellDict = [self cellDictionaryAtPosition:position];
		SGLabelTileView *cellToClone = cellDict[@"Cell"];
		cell = [cellToClone createClone];
	}

	return cell;
}


//_______________________________________________________________________________________________________________
//

-(void)foundWordsViewTapped
{
	[self hideFoundWords:nil];
}


//_______________________________________________________________________________________________________________
//

-(void)wordGridViewDoubleTapped
{
	[self checkPuzzle];
}


//_______________________________________________________________________________________________________________
//

-(void)checkPuzzle
{
	NSMutableArray *correctWords = [NSMutableArray array];
	NSInteger wordsFound = [self checkForWords:correctWords];
	if (wordsFound > 0)
	{
		[self animateWords:correctWords];
		[self.wordFoundSound play];

		for (SGWordSpec *wordSpec in correctWords)
		{
			_score += [wordSpec scoreForWord] * (wordsFound * 10);
		}
		self.scoreLabel.text = [[NSNumber numberWithInteger:_score] stringValue];
		self.endTime = [self.endTime dateByAddingTimeInterval:5 * wordsFound];
	}
	else
		[self.noWordSound play];
}


//_______________________________________________________________________________________________________________
//

- (void)timerFireMethod:(NSTimer *)timer
{
	// Create date from the elapsed time
	NSDate *currentDate = [NSDate date];
	NSTimeInterval timeInterval = [self.endTime timeIntervalSinceDate:currentDate];
	_solveTime = timeInterval;
	NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
	
	// Format the elapsed time and set it to the label
	NSString *timeString = [self.timerFormatter stringFromDate:timerDate];
	self.puzzleTimeLabel.text = timeString;

	if (timeInterval <= 0 && self.puzzleTimer.isValid)
	{
		[self.puzzleTimer invalidate];
		SGFinishedPuzzleViewController *finishedScreen = [[SGFinishedPuzzleViewController alloc] initWithNibName:@"SGFinishedPuzzleViewController" bundle:nil];
		finishedScreen.delegate = self;
		[self presentViewController:finishedScreen animated:YES completion:nil];
		[finishedScreen setFoundWords:[self getFoundWordsAsString]];
		[finishedScreen setScore:_score];
	}
}


//_______________________________________________________________________________________________________________
//

- (UIImage *)imageFromViewWithScale:(UIView *)inView scale:(CGFloat)scale
{
	// If scale is 0, it'll follows the screen scale for creating the bounds
	UIGraphicsBeginImageContextWithOptions(inView.bounds.size, NO, scale);
	[inView drawViewHierarchyInRect:inView.bounds afterScreenUpdates:YES];
	
	// Get the image out of the context
	UIImage *copied = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	// Return the result
	return copied;
}


//_______________________________________________________________________________________________________________
//

- (IBAction)showFoundWords:(id)sender
{
	[self.foundWordsView setWithFoundWords:[self getFoundWordsAsString]];
	[UIView transitionWithView:self.view
					  duration:0.3
					   options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
					animations:^{ self.foundWordsView.hidden = NO; }
					completion:NULL];
}


//_______________________________________________________________________________________________________________
//

- (IBAction)hideFoundWords:(id)sender
{
	[UIView transitionWithView:self.view
					  duration:0.3
					   options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
					animations:^{ self.foundWordsView.hidden = YES; }
					completion:NULL];
}


//_______________________________________________________________________________________________________________
//

- (NSString *)getFoundWordsAsString
{
	NSMutableString *foundWordsString = [NSMutableString stringWithString:@""];
	for (NSString *fword in self.usedWords)
	{
		[foundWordsString appendFormat:@"%@, ", fword];
	}
	
	NSUInteger strLen = foundWordsString.length;
	if (strLen > 0)
	{
		[foundWordsString deleteCharactersInRange:NSMakeRange(strLen - 2, 2)];
	}
	
	return foundWordsString;
}


//_______________________________________________________________________________________________________________
//

#pragma mark - GridView delegate
- (SGSolutionValue)gridView:(SGGridView *)gridView didMoveCell:(SGLabelTileView *)cell fromPosition:(SGGridPosition)fromPosition toPosition:(SGGridPosition)toPosition
{
	NSInteger amount;
	NSInteger rowOrColMoved;
	BOOL moveVertically;
	
	[self.clickSound play];

	//moving vertically
	toPosition = [gridView normalizePosition:toPosition];
	if (toPosition.column == fromPosition.column)
	{
		moveVertically = YES;
		rowOrColMoved = toPosition.column;
		
		//How many places is the tile moved (can be negative!)
		amount = toPosition.row - fromPosition.row;
		NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:fromPosition];
		NSMutableDictionary *toCell;
		do
		{
			//Get the next cell
			toCell = [self cellDictionaryAtPosition:toPosition];
			
			//update the current cell
			cellDict[@"Row"] = @(toPosition.row);
			
			//prepare the next cell
			cellDict = toCell;
			
			//calculate the next position
			toPosition.row += amount;
			
			toPosition = [gridView normalizePosition:toPosition];
		} while (toCell);
	}
	else //moving horizontally
	{
		moveVertically = NO;
		rowOrColMoved = toPosition.row;
		
		//How many places is the tile moved (can be negative!)
		amount = toPosition.column - fromPosition.column;
		NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:fromPosition];
		NSMutableDictionary *toCell;
		do
		{
			//Get the next cell
			toCell = [self cellDictionaryAtPosition:toPosition];
			
			//update the current cell
			cellDict[@"Column"] = @(toPosition.column);
			
			//prepare the next cell
			cellDict = toCell;
			
			//calculate the next position
			toPosition.column += amount;
			toPosition = [gridView normalizePosition:toPosition];
		} while (toCell);
		
	}
	
	return SGSolutionIcy;
}


//_______________________________________________________________________________________________________________
//

- (NSInteger)checkForWords:(NSMutableArray *)foundWords
{
	NSInteger newWordsCount = 0;
	
	for (NSInteger col = 0; col < _cachedColumns; col++)
	{
		NSString *columnWord = [self wordForColumn:col];
		if ([((SGWordPuzzle *)self.currPuzzle) checkAWord:columnWord])
		{
			BOOL isNew = [self maybeCacheNewWord:columnWord];
			SGWordSpec *newWord = [[SGWordSpec alloc] initInWord:columnWord inIndex:col inFlag:NO inNew:isNew];
			if (isNew) newWordsCount++;
			[foundWords addObject:newWord];
		}
	}
	for (NSInteger row = 0; row < _cachedRows; row++)
	{
		NSString *rowWord = [self wordForRow:row];
		if ([((SGWordPuzzle *)self.currPuzzle) checkAWord:rowWord])
		{
			BOOL isNew = [self maybeCacheNewWord:rowWord];
			SGWordSpec *newWord = [[SGWordSpec alloc] initInWord:rowWord inIndex:row inFlag:YES inNew:isNew];
			[foundWords addObject:newWord];
			if (isNew) newWordsCount++;
		}
	}
	
	return newWordsCount;
}


//_______________________________________________________________________________________________________________
//

- (NSArray *)getChangedWords:(BOOL)movedVertically indexMoved:(NSInteger)rowOrColumnIndex
{
	NSMutableArray *changedWords = [[NSMutableArray alloc] init];
	for (NSInteger col = 0; col < _cachedColumns; col++)
	{
		NSString *columnWord = [self wordForColumn:col];
		[changedWords addObject:columnWord];
	}
	for (NSInteger row = 0; row < _cachedRows; row++)
	{
		NSString *rowWord = [self wordForRow:row];
		[changedWords addObject:rowWord];
	}
	return changedWords;
}


//_______________________________________________________________________________________________________________
//

- (void)animateWords:(NSArray *)correctWords
{
	for (SGWordSpec *wordSpec in correctWords)
	{
		NSArray *viewsForIndex;
		if (wordSpec.isRow)
			viewsForIndex = [self tilesForRow:wordSpec.rowColIndex];
		else
			viewsForIndex = [self tilesForColumn:wordSpec.rowColIndex];

		CGRect wordRect = ((UIView *)viewsForIndex[0]).frame;
		for (UIView *tileView in viewsForIndex) {
			wordRect = CGRectUnion(wordRect, tileView.frame);
		}
		
		wordRect = [self.puzzleView convertRect:wordRect toView:nil];

		UILabel *scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 500.0, 50.0)];
		scoreLabel.text = [NSString stringWithFormat:@"%ld", (long)[wordSpec scoreForWord]];
		scoreLabel.textAlignment = NSTextAlignmentCenter;
		scoreLabel.font = [UIFont systemFontOfSize:34.0];
		scoreLabel.textColor = [UIColor colorWithRed:0.000 green:0.502 blue:0.000 alpha:1.000];
		scoreLabel.backgroundColor = [UIColor whiteColor];
		[scoreLabel sizeToFit];
		scoreLabel.frame = CGRectMake(0.0, 0.0, scoreLabel.frame.size.width + 10, scoreLabel.frame.size.height);

		CGPoint scoreOrigin = {wordRect.origin.x + (wordRect.size.width/2.0 - scoreLabel.frame.size.width/2.0),
							   wordRect.origin.y + (wordRect.size.height/2.0 - scoreLabel.frame.size.height/2.0)};
		CGRect scoreRect = {scoreOrigin, scoreLabel.frame.size};
		scoreLabel.frame = scoreRect;
		[self.view addSubview:scoreLabel];

		[UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
						 animations: ^{
							 scoreLabel.center = CGPointMake(scoreLabel.center.x, scoreLabel.center.y - 30.0);
							 scoreLabel.alpha = 0.01;
							 for (SGLabelTileView *letterView in viewsForIndex)
							 {
								 letterView.drawState = SGDrawCorrect;
							 }
						 }
		 
						 completion: ^(BOOL finished) {
							 [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut
											  animations: ^{
												  for (SGLabelTileView *letterView in viewsForIndex)
												  {
													  letterView.drawState = SGDrawNormal;
												  }
											  }
							  
											  completion: ^(BOOL finished) {
												  [scoreLabel removeFromSuperview];
											  }
							  ];
						 }
		 ];
	}
}


//_______________________________________________________________________________________________________________
//

- (NSString *)wordForRow:(NSInteger)row
{
	NSString *word = @"";
	for (NSInteger col = 0; col < _cachedColumns; col++)
	{
		NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:SGGridPositionMake(row,col)];
		SGLabelTileView *cell = cellDict[@"Cell"];
		word = [word stringByAppendingString:cell.tileChar.text];
	}
	return word;
}


//_______________________________________________________________________________________________________________
//

- (NSString *)wordForColumn:(NSInteger)col
{
	NSString *word = @"";
	for (NSInteger row = 0; row < _cachedRows; row++)
	{
		NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:SGGridPositionMake(row,col)];
		SGLabelTileView *cell = cellDict[@"Cell"];
		word = [word stringByAppendingString:cell.tileChar.text];
	}
	return word;
}


//_______________________________________________________________________________________________________________
//

- (NSArray *)tilesForRow:(NSInteger)row
{
	NSMutableArray *rowTiles = [NSMutableArray array];
	for (NSInteger col = 0; col < _cachedColumns; col++)
	{
		NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:SGGridPositionMake(row,col)];
		SGLabelTileView *cell = cellDict[@"Cell"];
		[rowTiles addObject:cell];
	}
	return rowTiles;
}


//_______________________________________________________________________________________________________________
//

- (NSArray *)tilesForColumn:(NSInteger)col
{
	NSMutableArray *colTiles = [NSMutableArray array];
	for (NSInteger row = 0; row < _cachedRows; row++)
	{
		NSMutableDictionary *cellDict = [self cellDictionaryAtPosition:SGGridPositionMake(row,col)];
		SGLabelTileView *cell = cellDict[@"Cell"];
		[colTiles addObject:cell];
	}
	return colTiles;
}


//_______________________________________________________________________________________________________________
//

- (BOOL)maybeCacheNewWord:(NSString *)newWord
{
	BOOL didCache = NO;
	if ([self.usedWords containsObject:newWord] == NO)
	{
		[self.usedWords addObject:newWord];
		didCache = YES;
	}
	return didCache;
}


//_______________________________________________________________________________________________________________
//

#pragma mark - Private methods
- (SGGridPosition)normalizePosition:(SGGridPosition)position inGridView:(SGGridView *)gridView
{
	return [gridView normalizePosition:position];
}


#pragma mark - Public methods
//_______________________________________________________________________________________________________________
//

- (NSMutableDictionary *)cellDictionaryAtPosition:(SGGridPosition)position
{
	//    position = [self normalizePosition:position inGridView:puzzleView];
	for (NSMutableDictionary *cellDict in _puzzleCells)
	{
		if([cellDict[@"Row"] intValue] == position.row)
		{
			if([cellDict[@"Column"] intValue] == position.column)
			{
				return cellDict;
			}
			else
			{
				continue;
			}
		}
		else
		{
			continue;
		}
	}
	return nil;
}


//_______________________________________________________________________________________________________________
//


- (void)startPuzzle
{
	_score = 0;
	[self.usedWords removeAllObjects];
	self.scoreLabel.text = @"0";
	[self setPuzzleData];					// save puzzle data to our dictionary of cells.
	[self.puzzleView reloadData];

	if (_gameTimeInterval > 0)
	{
		self.puzzleTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
															target:self
														  selector:@selector(timerFireMethod:)
														  userInfo:nil
														   repeats:YES];
		self.puzzleTimer.tolerance = 0.1;
		self.endTime = [NSDate dateWithTimeIntervalSinceNow:_gameTimeInterval];
	}
}


//_______________________________________________________________________________________________________________
// SGFinishedPuzzleViewController delegate methods

- (void)replayPuzzleChosen
{
	[self dismissViewControllerAnimated:YES completion:nil];
	[self startPuzzle];
}


//_______________________________________________________________________________________________________________
//

- (void)nextPuzzleChosen
{
	self.puzzleLevel++;
	
	[self.currPuzzle generateLevel:self.puzzleLevel];			// generate a puzzle
	
	[self.currPuzzle scramble];				// mix up puzzle cells
	[self dismissViewControllerAnimated:YES completion:nil];
	[self startPuzzle];
}


//_______________________________________________________________________________________________________________
//

- (void)mainMenuChosen
{
	[self dismissViewControllerAnimated:YES completion:nil];
	[self.navigationController popViewControllerAnimated:NO];
}

//_______________________________________________________________________________________________________________
//

- (NSInteger)lastLevelPlayed
{
	return self.puzzleLevel;
}


//_______________________________________________________________________________________________________________
//

@end
