//
//  SGFoundWordsView.swift
//  Scramble
//
//  Created by John Ojanen on 12/30/15.
//  Copyright © 2015 John Ojanen. All rights reserved.
//


import UIKit


class SGFoundWordsView: UIView {

	var wordList : UITextView
	
	fileprivate let kTopMargin: CGFloat = 50.0
	fileprivate let kBottomMargin: CGFloat = 80.0
	fileprivate let kLeftMargin: CGFloat = 20.0
	fileprivate let kRightMargin: CGFloat = 20.0

	override init(frame: CGRect)
	{
		let effectView = UIVisualEffectView.init(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
		effectView.frame = frame;
		
		var solvedRect = CGRect.init()
		solvedRect.origin.x = 0.0
		solvedRect.origin.y = 0.0
		solvedRect.size.width = frame.size.width - (kLeftMargin + kRightMargin)
		solvedRect.size.height = frame.size.height - (kTopMargin + kBottomMargin)

		self.wordList = UITextView.init(frame: solvedRect)
		self.wordList.isEditable = false
		self.wordList.font = UIFont.systemFont(ofSize: 20.0)
		effectView.addSubview(self.wordList)

		let insets : UIEdgeInsets = UIEdgeInsets.init(top: kTopMargin, left: kLeftMargin, bottom: kBottomMargin, right: kRightMargin)
		self.wordList.autoPinEdgesToSuperviewEdges(with: insets)
		super.init(frame: frame);
		self.translatesAutoresizingMaskIntoConstraints = false;
	}

	
	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	
	@objc open func set(foundWords : String)
	{
		wordList.text = foundWords
	}

	
	/*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
