//
//  NSArray+Shuffle.h
//  Scramble
//
//  Created by John Ojanen on 11/11/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <Foundation/NSArray.h>


@interface NSMutableArray (ArchUtils_Shuffle)

- (void)seedShuffle:(unsigned)seedVal;
- (void)shuffle;

@end
