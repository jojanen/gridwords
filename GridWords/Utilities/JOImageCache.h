//
//  JOImageCache.h
//  Scramble
//
//  Created by jojanen on 8/19/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface JOImageCache : NSObject {
    
}

- (NSData *)cacheImage:(NSURL *)imageURLString;
- (UIImage *)getImage:(NSURL *)imageURLString;

+ (void)clearImageCache;

@end
