//
//  SIImageCache.m
//  FieldNationiOS
//
//  Created by jojanen on 8/19/14.
//  Copyright (c) 2014 FieldNation. All rights reserved.
//

#import "JOImageCache.h"

#define TMP NSTemporaryDirectory()

@implementation JOImageCache


//________________________________________________________________________________
//

- (NSData *)cacheImage:(NSURL*)imageURL
{
    NSString *imageURLString = [imageURL absoluteString];
    
    //generating unique name for the cached file with ImageURLString so you can retrive it back
    NSMutableString *tmpStr = [NSMutableString stringWithString:imageURLString];
    [tmpStr replaceOccurrencesOfString:@"/" withString:@"-" options:1 range:NSMakeRange(0, [tmpStr length])];
    
    NSString *filename = [NSString stringWithFormat:@"%@",tmpStr];
    
    NSString *uniquePath = [TMP stringByAppendingPathComponent: filename];
    
    // Check for file existence
    if (![[NSFileManager defaultManager] fileExistsAtPath: uniquePath])
    {
        // The file doesn't exist, we should get a copy of it
        
        // Fetch image
        NSData *data = [[NSData alloc] initWithContentsOfURL: imageURL];
        UIImage *image = [[UIImage alloc] initWithData: data];
        
        if(image == nil)
            return nil;
        
        // Is it PNG or JPG/JPEG?
        // Running the image representation function writes the data from the image to a file
        if([imageURLString rangeOfString: @".png" options: NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [UIImagePNGRepresentation(image) writeToFile: uniquePath atomically: YES];
        }
        else if(
                [imageURLString rangeOfString: @".JPG" options: NSCaseInsensitiveSearch].location != NSNotFound ||
                [imageURLString rangeOfString: @".jpg" options: NSCaseInsensitiveSearch].location != NSNotFound ||
                [imageURLString rangeOfString: @".jpeg" options: NSCaseInsensitiveSearch].location != NSNotFound
                )
        {
            [UIImageJPEGRepresentation(image, 100) writeToFile:uniquePath atomically:YES];
        }
        
        return data;
    }
    
    return nil;
}


//________________________________________________________________________________
//

- (UIImage *)getImage:(NSURL *)imageURL
{
    NSString *imageURLString = [imageURL absoluteString];
    NSMutableString *tmpStr = [NSMutableString stringWithString:imageURLString];
    [tmpStr replaceOccurrencesOfString:@"/" withString:@"-" options:1 range:NSMakeRange(0, [tmpStr length])];
    
    NSString *filename = [NSString stringWithFormat:@"%@",tmpStr];
    NSString *uniquePath = [TMP stringByAppendingPathComponent: filename];
    
    UIImage *image;
    
    // Check for a cached version
    if ([[NSFileManager defaultManager] fileExistsAtPath:uniquePath])
    {
        NSData *data = [[NSData alloc] initWithContentsOfFile:uniquePath];
        image = [[UIImage alloc] initWithData:data] ; // this is the cached image
    }
    else
    {
        // get a new one
        NSData *data = [self cacheImage: imageURL];
        image = [[UIImage alloc] initWithData:data];
    }
    
    return image;
}


//________________________________________________________________________________
//

+(void)clearImageCache
{
    NSString *tempPath = NSTemporaryDirectory();
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:nil];
    NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.JPG'"]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (onlyJPGs) {
        for (int i = 0; i < [onlyJPGs count]; i++) {
            NSLog(@"Directory Count: %lu", (unsigned long)[onlyJPGs count]);
            NSString *contentsOnly = [NSString stringWithFormat:@"%@%@", tempPath, [onlyJPGs objectAtIndex:i]];
            [fileManager removeItemAtPath:contentsOnly error:nil];
        }
    }

}


@end