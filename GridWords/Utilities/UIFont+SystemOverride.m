//
//  UIFont+SystemOverride.m
//  JohnDoh
//
//  Created by jojanen on 12/2/14.
//  Copyright (c) 2014 JohnDoh. All rights reserved.
//

#import "UIFont+SystemOverride.h"


@implementation UIFont (SytemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"


+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Raleway-Bold" size:fontSize];
}


+ (UIFont *)systemFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Raleway" size:fontSize];
}


+ (UIFont *)italicSystemFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Raleway-Italic" size:fontSize];
}

#pragma clang diagnostic pop


@end
