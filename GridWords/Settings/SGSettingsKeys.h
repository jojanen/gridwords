//
//  SGSettingsKeys.h
//  Scramble
//
//  Created by John Ojanen on 4/12/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//


#define kGridSizeX @"gridSizeX"
#define kGridSizeY @"gridSizeY"
#define kGameTime @"gameTime"
#define kPuzzleOneLevel @"puzzleOneLevel"

