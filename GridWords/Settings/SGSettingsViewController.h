//
//  SGSettingsViewController.h
//  Scramble
//
//  Created by John Ojanen on 3/22/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SGSettingsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *rowsLabel;
@property (strong, nonatomic) IBOutlet UILabel *colsLabel;
@property (strong, nonatomic) IBOutlet UIStepper *rowStepper;
@property (strong, nonatomic) IBOutlet UIStepper *colStepper;

- (IBAction)GoBack:(id)sender;

@end
