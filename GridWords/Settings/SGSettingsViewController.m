//
//  SGSettingsViewController.m
//  Scramble
//
//  Created by John Ojanen on 3/22/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import "SGSettingsViewController.h"
#import "SGSettingsKeys.h"


@interface SGSettingsViewController ()
{

	NSUserDefaults* userDefaults;
}

- (void)rowStepperAction:(id)sender;
- (void)colStepperAction:(id)sender;

@end


@implementation SGSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self.rowStepper addTarget:self action:@selector(rowStepperAction:) forControlEvents:UIControlEventValueChanged];
	[self.colStepper addTarget:self action:@selector(colStepperAction:) forControlEvents:UIControlEventValueChanged];
	
	userDefaults = [NSUserDefaults standardUserDefaults];
	self.rowStepper.minimumValue = 2;
	self.rowStepper.maximumValue = 6;
	self.rowStepper.value = [userDefaults integerForKey:kGridSizeY];
	self.colStepper.minimumValue = 2;
	self.colStepper.maximumValue = 6;
	self.colStepper.value = [userDefaults integerForKey:kGridSizeX];
	self.rowsLabel.text = [NSString stringWithFormat:@"%ld",lround(self.rowStepper.value)];
	self.colsLabel.text = [NSString stringWithFormat:@"%ld",lround(self.colStepper.value)];
}


- (void)viewWillDisappear:(BOOL)animated
{
	// remove the target/action for a set of events. pass in NULL for the action to remove all actions for that target
	[self.rowStepper removeTarget:self action:@selector(rowStepperAction:) forControlEvents:UIControlEventValueChanged];
	[self.colStepper removeTarget:self action:@selector(colStepperAction:) forControlEvents:UIControlEventValueChanged];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)rowStepperAction:(id)sender
{
	UIStepper *localRowStepper = (UIStepper *)sender;
	NSInteger newVal = lround(localRowStepper.value);
	self.rowsLabel.text = [NSString stringWithFormat:@"%lu", (long)newVal];
	[userDefaults setInteger:newVal forKey:kGridSizeY];
}


- (void)colStepperAction:(id)sender
{
	UIStepper *localColStepper = (UIStepper *)sender;
	NSInteger newVal = lround(localColStepper.value);
	self.colsLabel.text = [NSString stringWithFormat:@"%lu", (long)newVal];
	[userDefaults setInteger:newVal forKey:kGridSizeX];
}


- (IBAction)GoBack:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


@end
