//
//  SGViewController.h
//  Scramble
//
//  Created by John Ojanen on 3/2/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGMMenuController : UIViewController

- (IBAction)playGame:(id)sender;
- (IBAction)playColorsPuzzile:(id)sender;
- (IBAction)showSettings:(id)sender;

@end
