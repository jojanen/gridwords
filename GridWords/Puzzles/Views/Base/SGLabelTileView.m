//
//  SGTileView.m
//  Scramble
//
//  Created by John Ojanen on 3/4/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGLabelTileView.h"
#import "PureLayout.h"
#import "UIFont+SystemOverride.h"

@interface SGLabelTileView ()

@property (nonatomic) UIView *innerTileView;

@end


@implementation SGLabelTileView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.backgroundColor = [UIColor clearColor];
		
		CGRect innerFrame = CGRectInset(frame, 1.0, 1.0);
		self.innerTileView = [[UIView alloc] initWithFrame:innerFrame];
		self.innerTileView.backgroundColor = [UIColor colorWithRed:0.557 green:0.643 blue:0.753 alpha:1.000];

		UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, 45.0, 20.0)];
		tempLabel.textAlignment = NSTextAlignmentCenter;
		[self.innerTileView addSubview:tempLabel];
		[tempLabel autoSetDimensionsToSize:CGSizeMake(45.0, 35.0)];
		[tempLabel autoCenterInSuperview];

		[self addSubview:self.innerTileView];
		ALEdgeInsets innerTileInsets = {1.0, 1.0, 1.0, 1.0};
		[self.innerTileView autoPinEdgesToSuperviewEdgesWithInsets:innerTileInsets];
		
		self.tileChar = tempLabel;
		self.tileChar.font = [UIFont fontWithName:@"Raleway" size:30.0];

	}
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    // Drawing code
//	CGContextRef context = UIGraphicsGetCurrentContext();
//
//	CGContextSetStrokeColorWithColor(context, self.tileColor.CGColor);
////	CGRect insetRect = CGRectInset(rect, 1.0, 1.0);
//	CGContextStrokeRectWithWidth(context, rect, 1.0);
//}


// tiles must override this. Creates new SGTileView with same content as inView.

- (instancetype)createClone
{
	SGLabelTileView *clonedView = [[SGLabelTileView alloc] initWithFrame:self.frame];
	clonedView.backgroundColor = self.backgroundColor;
	clonedView.tileChar.text = self.tileChar.text;
	return clonedView;
}


- (void)setDrawState:(SGTileDrawState)inState
{
	[super setDrawState:inState];
	if (inState == SGDrawNormal)
	{
		self.innerTileView.backgroundColor = [UIColor colorWithRed:0.557 green:0.643 blue:0.753 alpha:1.000];
	}
	else if (inState == SGDrawCorrect)
	{
		self.innerTileView.backgroundColor = [UIColor colorWithRed:0.000 green:0.502 blue:0.251 alpha:1.000];
	}
}


@end
