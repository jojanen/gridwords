//
//  SGTileView.m
//  Scramble
//
//  Created by John Ojanen on 11/11/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGTileView.h"

@implementation SGTileView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)createClone
{
	return nil;
}


- (void)setDrawState:(SGTileDrawState)inState
{
	_drawState = inState;
}

@end
