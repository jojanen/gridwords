//
//  SGTileView.h
//  Scramble
//
//  Created by John Ojanen on 11/11/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, SGTileDrawState) {
    SGDrawNormal,
    SGDrawCorrect,
    SGDrawWrong,
};
@interface SGTileView : UIView

@property (nonatomic) SGTileDrawState drawState;

- (instancetype)createClone;					// tiles must override this. Creates new SGTileView and copies values.

@end
