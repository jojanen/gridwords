//
//  SGTileView.h
//  Scramble
//
//  Created by John Ojanen on 3/4/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGTileView.h"


@interface SGLabelTileView : SGTileView

@property (weak, nonatomic) IBOutlet UILabel *tileChar;
@end
