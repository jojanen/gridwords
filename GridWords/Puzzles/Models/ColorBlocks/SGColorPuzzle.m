//
//  SGColorPuzzle.m
//  Scramble
//
//  Created by John Ojanen on 11/10/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGColorPuzzle.h"
#import "SGLabelTileView.h"
#import "NSArray+Shuffle.h"


@interface SGColorPuzzle ()

@property (nonatomic) NSMutableArray *tileGridArray;
@property (nonatomic) NSMutableArray *scrambledGridArray;

@end


@implementation SGColorPuzzle

//_______________________________________________________________________________________________________________
//

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount
{
	if (self = [super initWithRows:(rowCount) andColumns:(colCount)])
	{
		NSInteger puzzleSize = (self.rowCount) * (self.colCount);
		
		self.tileGridArray = [NSMutableArray arrayWithCapacity:puzzleSize];
	}
	return self;
}


//_______________________________________________________________________________________________________________
//

- (void)generateLevel:(NSInteger)level
{
	NSInteger halfHeight = self.rowCount / 2;
	NSInteger halfWidth = self.colCount / 2;
	
	for (NSInteger i = 0; i < halfHeight; i++)
	{
		for (NSInteger j = 0; j < self.colCount; j++)
		{
			if (j < halfWidth)
				[self.tileGridArray addObject:[UIColor colorWithRed:0.812 green:0.169 blue:0.035 alpha:1.000]];
			else
				[self.tileGridArray addObject:[UIColor colorWithRed:0.557 green:0.643 blue:0.753 alpha:1.000]];
		}
	}
	
	for (NSInteger i = halfHeight; i < self.rowCount; i++)
	{
		for (NSInteger j = 0; j < self.colCount; j++)
		{
			if (j < halfWidth)
				[self.tileGridArray addObject:[UIColor colorWithRed:0.757 green:0.765 blue:0.553 alpha:1.000]];
			else
				[self.tileGridArray addObject:[UIColor colorWithRed:0.149 green:0.149 blue:0.133 alpha:1.000]];
		}
	}
	self.scrambledGridArray = [NSMutableArray arrayWithArray:self.tileGridArray];
}


//_______________________________________________________________________________________________________________
//

- (void)scramble
{
	[self.scrambledGridArray shuffle];
}


//_______________________________________________________________________________________________________________
//

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY
{
	BOOL hasEntry = NO;
	
	NSUInteger tileIndex = iY * self.colCount + iX;
	
	if (self.scrambledGridArray && self.scrambledGridArray.count >= tileIndex)
	{
		NSNumber *tileEntry = (NSNumber *)[self.scrambledGridArray objectAtIndex:tileIndex];
		hasEntry = (tileEntry != NULL);
	}
	
	return hasEntry;
}


//_______________________________________________________________________________________________________________
//

- (SGLabelTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy
{
	SGLabelTileView *newTile = nil;
	
	if ([self hasEntryAtX:ix Y:iy])
	{
		CGRect tileRect = CGRectZero;
		newTile = [[SGLabelTileView alloc] initWithFrame:tileRect];
		
		NSUInteger tileIndex = iy * self.colCount + ix;
		
		UIColor *tileColor = (UIColor *)[self.scrambledGridArray objectAtIndex:tileIndex];
		newTile.backgroundColor = tileColor;
	}
	return newTile;
}


//_______________________________________________________________________________________________________________
//

- (SGSolutionValue)checkPuzzleSolution:(NSArray *)puzzleTiles
{
	SGSolutionValue closeness = SGSolutionIcy;
	NSInteger numberOfMatches = 0;
	
	for (NSInteger i = 0; i < (self.rowCount * self.colCount); i++)
	{
		SGLabelTileView *colorTile = [puzzleTiles objectAtIndex:i];
		UIColor *inColor = colorTile.backgroundColor;
		UIColor *solvedColor = [self.tileGridArray objectAtIndex:i];
		if ([inColor isEqual:solvedColor])
			numberOfMatches++;
	}
	
	NSInteger puzzleEntries = self.rowCount * self.colCount;
	if (numberOfMatches == puzzleEntries)
		closeness = SGSolutionGotIt;
	else if (numberOfMatches < puzzleEntries / 4)
		closeness = SGSolutionColder;
	else if (numberOfMatches < puzzleEntries / 2)
		closeness = SGSolutionCold;
	else if (numberOfMatches < puzzleEntries - puzzleEntries / 4)
		closeness = SGSolutionWarm;
	else if (numberOfMatches < puzzleEntries - 3)
		closeness = SGSolutionWarmer;
	else if (numberOfMatches >= puzzleEntries - 2)
		closeness = SGSolutionHot;
	
	return closeness;
}

@end
