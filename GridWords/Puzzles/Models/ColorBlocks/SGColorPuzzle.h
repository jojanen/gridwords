//
//  SGColorPuzzle.h
//  Scramble
//
//  Created by John Ojanen on 11/10/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGPuzzle.h"

@interface SGColorPuzzle : SGPuzzle

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount;
- (void)generateLevel:(NSInteger)level;
- (void)scramble;

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY;
- (SGTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy;

@end
