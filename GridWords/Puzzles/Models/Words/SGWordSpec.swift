//
//  SGWordSpec.swift
//  Scramble
//
//  Created by John Ojanen on 2/15/15.
//  Copyright (c) 2015 John Ojanen. All rights reserved.
//

import UIKit

class SGWordSpec: NSObject {
	
	@objc var isNew: Bool
	@objc var isRow: Bool
	@objc var rowColIndex: Int
	@objc var word: String;
	

	@objc init(inWord:String, inIndex:Int, inFlag:Bool, inNew:Bool) {
		self.word = inWord
		self.rowColIndex = inIndex
		self.isRow = inFlag
		self.isNew = inNew
	}
	

	@objc func scoreForWord() -> Int {
		let wordScore = SGWordPuzzle.score(forWord: self.word)
		
		return wordScore
	}

}
