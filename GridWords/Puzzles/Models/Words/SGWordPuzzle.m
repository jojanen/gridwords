//
//  SGWordPuzzle.m
//  Scramble
//
//  Created by John Ojanen on 4/23/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import "SGWordPuzzle.h"
#import "NSArray+Shuffle.h"
#import "SGLabelTileView.h"
#import "Lexicontext.h"

// Notes: ascii character range
// A = 65
// Z = 90
// a = 97
// z = 122

@interface SGWordPuzzle ()
{
	NSInteger _currLevel;
}

@property (nonatomic) NSMutableArray *tileGridArray;
@property (nonatomic) NSMutableArray *scrambledGridArray;
@property (nonatomic) UITextChecker *textChecker;
@property (nonatomic) NSString *savedLanguage;
@property (nonatomic) NSCharacterSet *vowelSet;
@property (nonatomic) NSCharacterSet *consonantSet;
@property (nonatomic) NSString *qAsStr;
@property (nonatomic) NSString *uAsStr;

- (const unichar)randomLetter;
- (const unichar)randomConsanant;
- (const unichar)randomVowel;

@end


//_______________________________________________________________________________________________________________
//


static NSString * const letters = @"nuihmquaeanegwngeehahspcolnhnrzaspffktstiydobjoabowtoatiotmucerttylryvdeltoessilreixdterwhveiunes";
static NSString * const consonants = @"bcdfghjklmnpqrstvwxyz";
static NSString * const vowels = @"aeiou";

static NSString * const boggleLetters1 = @"aeaneg";
static NSString * const boggleLetters2 = @"wngeeh";
static NSString * const boggleLetters3 = @"ahspco";
static NSString * const boggleLetters4 = @"lnhnrz";
static NSString * const boggleLetters5 = @"aspffk";
static NSString * const boggleLetters6 = @"tstiyd";
static NSString * const boggleLetters7 = @"objoab";
static NSString * const boggleLetters8 = @"owtoat";
static NSString * const boggleLetters9 = @"iotmuc";
static NSString * const boggleLetters10 = @"erttyl";
static NSString * const boggleLetters11 = @"ryvdel";
static NSString * const boggleLetters12 = @"toessi";
static NSString * const boggleLetters13 = @"lreixd";
static NSString * const boggleLetters14 = @"terwhv";
static NSString * const boggleLetters15 = @"eiunes";
static NSString * const boggleLetters16 = @"nuihmqu";

//										 a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q, r,s,t,u,v,w,x,y,z
static const NSInteger letterScores[] = {1,3,3,2,1,4,2,4,1,8,5,1,3,1,1,3,10,1,1,1,1,4,4,8,4,10};

//aaafrs aaeeee aafirs adennn aeeeem
//aeegmu aegmnn afirsy bjkqxz ccenst
//ceiilt ceilpt ceipst ddhnot dhhlor
//fiprsy gorrvw iprrry nootuw ooottu
//eiiitt emottt ensssu dhlnor dhlnor (duplicate)
//
//Another distribution not used on this page:
//
//AAAFRS AAEEEE AAFIRS ADENNN AEEEEM
//AEEGMU AEGMNN AFIRSY BJKQXZ CCNSTW
//CEIILT CEILPT CEIPST DHHNOT DHHLOR
//DHLNOR DDLNOR EIIITT EMOTTT ENSSSU
//FIPRSY GORRVW HIPRRY NOOTUW OOOTTU


//_______________________________________________________________________________________________________________
//


@implementation SGWordPuzzle


// RAND_MAX assumed to be 32767.
static unsigned long int next = 1;
void srand(unsigned int seed) { next = seed; }
int rand(void) {
	next = next * 1103515245 + 12345;
	return (unsigned int)(next/65536) % 32768;
}

//_______________________________________________________________________________________________________________
//

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount
{
	if (self = [super initWithRows:rowCount andColumns:colCount])
	{
		NSInteger puzzleSize = self.rowCount * self.colCount;
		
		self.tileGridArray = [NSMutableArray arrayWithCapacity:puzzleSize];
		
		self.textChecker = [[UITextChecker alloc] init];
		NSLocale *currentLocale = [NSLocale currentLocale];
		self.savedLanguage = [currentLocale objectForKey:NSLocaleLanguageCode];

		self.vowelSet = [NSCharacterSet characterSetWithCharactersInString:vowels];
		self.consonantSet = [NSCharacterSet characterSetWithCharactersInString:consonants];
		
		self.qAsStr = @"q";
		self.uAsStr = @"u";
		
	}
	return self;
}


//_______________________________________________________________________________________________________________
//

- (void)generateLevel:(NSInteger)level
{
	BOOL needsAU = NO;
	BOOL hasAU = NO;
	NSInteger vowelCount = 0;
	NSInteger lastVowelIndex = -1;
	
	NSInteger tileCount = self.rowCount * self.colCount;
	NSInteger minimumVowels = self.rowCount;

	_currLevel = level;
	srand(42 * (unsigned int)_currLevel);

	for (NSInteger i = 0; i < tileCount; i++)
	{
		unichar rletter = [self randomLetter];

		NSString *newChar = [NSString stringWithCharacters:&rletter length:1];
		
		if ([self.vowelSet characterIsMember:rletter])
		{
			vowelCount += 1;
			lastVowelIndex = i;
			if ([newChar isEqualToString:self.uAsStr])
				hasAU = YES;
		}
		
		if ([newChar isEqualToString:self.qAsStr])
			needsAU = YES;
		
		[self.tileGridArray addObject:[NSString stringWithCharacters:&rletter length:1]];
	}
	
	if (needsAU && !hasAU)
	{
		[self.tileGridArray replaceObjectAtIndex:lastVowelIndex withObject:@"u"];
	}
	
	if (vowelCount < minimumVowels)
	{
		NSInteger vowelsToAdd = minimumVowels - vowelCount;
		BOOL done = NO;
		while (!done)
		{
			NSInteger randIndex = [self getRandFrom:0 to:tileCount];
			NSString *charToReplace = [self.tileGridArray objectAtIndex:randIndex];
			if ([self.consonantSet characterIsMember:[charToReplace characterAtIndex:0]])
			{
				unichar newLetter = [self randomVowel];
				[self.tileGridArray replaceObjectAtIndex:randIndex withObject:[NSString stringWithCharacters:&newLetter length:1]];
				vowelsToAdd -= 1;
			}
			
			done = (vowelsToAdd <= 0);
		}
	}
	
	self.scrambledGridArray = [NSMutableArray arrayWithArray:self.tileGridArray];
}


//_______________________________________________________________________________________________________________
//

- (void)scramble
{
	[self.scrambledGridArray seedShuffle:42 * (unsigned int)_currLevel];
	[self.scrambledGridArray shuffle];
}


//_______________________________________________________________________________________________________________
//

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY
{
	BOOL hasEntry = NO;
	
	NSUInteger tileIndex = iY * self.colCount + iX;
	
	if (self.scrambledGridArray && self.scrambledGridArray.count >= tileIndex)
	{
		NSNumber *tileEntry = (NSNumber *)[self.scrambledGridArray objectAtIndex:tileIndex];
		hasEntry = (tileEntry != NULL);
	}
	
	return hasEntry;
}


//_______________________________________________________________________________________________________________
//

- (SGLabelTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy
{
	SGLabelTileView *newTile = nil;
	
	if ([self hasEntryAtX:ix Y:iy])
	{
		CGRect tileRect = CGRectZero;
		newTile = [[SGLabelTileView alloc] initWithFrame:tileRect];
		
		NSUInteger tileIndex = iy * self.colCount + ix;
		
		NSString *tileEntry = (NSString *)[self.scrambledGridArray objectAtIndex:tileIndex];
		newTile.tileChar.text = tileEntry;
	}
	return newTile;
}


//_______________________________________________________________________________________________________________
//

- (BOOL)checkAWord:(NSString *)word
{
	Lexicontext *dictionary = [Lexicontext sharedDictionary];
	BOOL hasDefinition = [dictionary containsDefinitionFor:word];
	return hasDefinition;
}


//_______________________________________________________________________________________________________________
//

- (NSMutableArray *)checkWords:(NSArray *)changedWords
{
	NSMutableArray *correctWords = [[NSMutableArray alloc] init];
	
	for (NSInteger i = 0; i < changedWords.count; i++)
	{
		NSString *nextWord = changedWords[i];
		if ([self checkAWord:nextWord])
			[correctWords addObject:nextWord];
	}
	return correctWords;
}


//_______________________________________________________________________________________________________________
//

-(bool)isRealWordOK:(NSString *)wordToCheck
{
	
	// we dont want to use any words that the lexicon has learned.
	if ([UITextChecker hasLearnedWord:wordToCheck])
	{
		return false;
	}
	
	return true;

	// now we are going to use the word completion function to see if this word really exists, by removing the final letter and then asking auto complete to complete the word, then look through all the results and if its not found then its not a real word. Note the auto complete is very acurate unlike the spell checker.
//	NSRange range = NSMakeRange(0, wordToCheck.length - 1);
//	NSArray *guesses = [self.textChecker completionsForPartialWordRange:range inString:wordToCheck language:self.savedLanguage];
//	
//	// confirm that the word is found in the auto complete list
//	for (NSString *guess in guesses) {
//		
//		if ([guess isEqualToString:wordToCheck]) {
//			// we found the word in the auto complete list so it's real :-)
//			return true;
//		}
//	}
//	
//	// if we get to here then it's not a real word :-(
//	NSLog(@"Word not found in second dictionary check:%@",wordToCheck);
//	return false;
	
}
  

//_______________________________________________________________________________________________________________
//

+ (NSInteger)scoreForWord:(NSString *)inWord
{
	NSInteger score = 0;
	
	NSUInteger strLen = inWord.length;
	const char *wordPtr = inWord.UTF8String;
	for (NSUInteger i = 0; i < strLen; i++)
	{
		char currLetter = wordPtr[i];
		score += letterScores[currLetter - 'a'];
	}
	
	return score;
}


//_______________________________________________________________________________________________________________
//

-(NSInteger) getRandFrom:(NSInteger)min to:(NSInteger)max
{
	return (rand() % (max - min)) + min;

//	static u_int32_t const kUpperLimit = 1000;
//	return (arc4random_uniform(kUpperLimit) % (max - min)) + min;
}


//_______________________________________________________________________________________________________________
//

-(NSString *) genRandStringOfLength:(int)len
{
	
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
	
    for (int i=0; i<len; i++) {
		[randomString appendFormat: @"%C", [letters characterAtIndex:rand() % [letters length]]];
    }
	
    return randomString;
}


//_______________________________________________________________________________________________________________
//

- (const unichar)randomLetter
{
	unichar randomChar = [letters characterAtIndex: rand() % [letters length]];
	return randomChar;
}


//_______________________________________________________________________________________________________________
//

- (const unichar)randomConsanant
{
	unichar randomConsonant = [consonants characterAtIndex:rand() % [consonants length]];
	return randomConsonant;
}


//_______________________________________________________________________________________________________________
//

- (const unichar)randomVowel
{
	unichar randomVowel = [vowels characterAtIndex:rand() % [vowels length]];
	return randomVowel;
}


@end


// Code to use built in iOS spell check. It sucks.
//	BOOL isCorrectlySpelled = NO;
//
//	NSRange vowelRange = [word rangeOfCharacterFromSet:self.vowelSet];
//	if (vowelRange.length > 0)
//	{
//		NSRange wordRange = NSMakeRange(0, word.length);
//		NSRange misspelledRange = [self.textChecker rangeOfMisspelledWordInString:word range:wordRange startingAt:0 wrap:NO language:self.savedLanguage];
//		if (misspelledRange.location == NSNotFound && [self isRealWordOK:word])
//			isCorrectlySpelled = YES;
//	}
//	return isCorrectlySpelled;
