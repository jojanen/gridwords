//
//  SGWordPuzzle.h
//  Scramble
//
//  Created by John Ojanen on 4/23/13.
//  Copyright (c) 2013 John Ojanen. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SGPuzzle.h"

@interface SGWordPuzzle : SGPuzzle
{
	
}

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount;
- (void)generateLevel:(NSInteger)level;
- (void)scramble;

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY;
- (SGTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy;

- (NSMutableArray *)checkWords:(NSArray *)changedWords;
- (BOOL)checkAWord:(NSString *)word;

+ (NSInteger)scoreForWord:(NSString *)inWord;

@end
