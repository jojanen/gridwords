//
//  SGNumberPuzzle.m
//  Scramble
//
//  Created by John Ojanen on 11/10/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGNumberPuzzle.h"
#import "NSArray+Shuffle.h"
#import "SGLabelTileView.h"


@interface SGNumberPuzzle ()

@property (nonatomic) NSMutableArray *tileGridArray;
@property (nonatomic) NSMutableArray *scrambledGridArray;

@end


@implementation SGNumberPuzzle


//_______________________________________________________________________________________________________________
//

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount
{
	if (self = [super initWithRows:rowCount andColumns:colCount])
	{
		NSInteger puzzleSize = self.rowCount * self.colCount;
		
		self.tileGridArray = [NSMutableArray arrayWithCapacity:puzzleSize];
	}
	return self;
}


//_______________________________________________________________________________________________________________
//

- (void)generateLevel:(NSInteger)level
{
	for (NSInteger i = 0; i < (self.rowCount * self.colCount); i++)
	{
		[self.tileGridArray addObject:[NSNumber numberWithInteger:i]];
	}
	
	self.scrambledGridArray = [NSMutableArray arrayWithArray:self.tileGridArray];
}


//_______________________________________________________________________________________________________________
//

- (void)scramble
{
	[self.scrambledGridArray shuffle];
}


//_______________________________________________________________________________________________________________
//

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY
{
	BOOL hasEntry = NO;
	
	NSUInteger tileIndex = iY * self.colCount + iX;
	
	if (self.scrambledGridArray && self.scrambledGridArray.count >= tileIndex)
	{
		NSNumber *tileEntry = (NSNumber *)[self.scrambledGridArray objectAtIndex:tileIndex];
		hasEntry = (tileEntry != NULL);
	}
	
	return hasEntry;
}


//_______________________________________________________________________________________________________________
//

- (SGLabelTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy
{
	SGLabelTileView *newTile = nil;
	
	if ([self hasEntryAtX:ix Y:iy])
	{
		CGRect tileRect = CGRectZero;
		newTile = [[SGLabelTileView alloc] initWithFrame:tileRect];
	
		NSUInteger tileIndex = iy * self.colCount + ix;

		NSNumber *tileEntry = (NSNumber *)[self.scrambledGridArray objectAtIndex:tileIndex];
		newTile.tileChar.text = [tileEntry stringValue];
	}
	return newTile;
}


//_______________________________________________________________________________________________________________
//

- (SGSolutionValue)checkPuzzleSolution:(NSArray *)puzzleTiles
{
	SGSolutionValue closeness = SGSolutionIcy;
	NSInteger numberOfMatches = 0;

	for (NSInteger i = 0; i < (self.rowCount * self.colCount); i++)
	{
		SGLabelTileView *numberTile = [puzzleTiles objectAtIndex:i];
		NSInteger tileValue = [numberTile.tileChar.text integerValue];
		NSNumber *solvedEntry = [self.tileGridArray objectAtIndex:i];
		if (tileValue == [solvedEntry integerValue])
			numberOfMatches++;
	}
	
	NSInteger puzzleEntries = self.rowCount * self.colCount;
	if (numberOfMatches == puzzleEntries)
		closeness = SGSolutionGotIt;
	else if (numberOfMatches < puzzleEntries / 4)
		closeness = SGSolutionColder;
	else if (numberOfMatches < puzzleEntries / 2)
		closeness = SGSolutionCold;
	else if (numberOfMatches < puzzleEntries - puzzleEntries / 4)
		closeness = SGSolutionWarm;
	else if (numberOfMatches < puzzleEntries - 3)
		 closeness = SGSolutionWarmer;
	else if (numberOfMatches >= puzzleEntries - 2)
		closeness = SGSolutionHot;
	
	return closeness;
}


@end
