//
//  SGTile.h
//  
//
//  Created by John Ojanen on 1/11/14.
//
//

#import <Foundation/Foundation.h>

@interface SGPuzzleTile : NSObject

@property (readonly) NSUInteger tileIndex;
@property (readonly) NSInteger tileID;

@end
