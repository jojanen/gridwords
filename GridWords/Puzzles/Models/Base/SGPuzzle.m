//
//  SGPuzzle.m
//  
//
//  Created by John Ojanen on 1/11/14.
//
//

#import "SGPuzzle.h"
#import "SGTileView.h"


@interface SGPuzzle (private)

@property (strong, nonatomic) NSMutableArray *tileGrid;

@end


@implementation SGPuzzle

//_______________________________________________________________________________________________________________
//

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount
{
	if (self = [super init])
	{
		_rowCount = rowCount;
		_colCount = colCount;
	}
	return self;
}


//_______________________________________________________________________________________________________________
//

- (void)generateLevel:(NSInteger)level
{
	
}


//_______________________________________________________________________________________________________________
//

- (void)scramble
{
	
}


//_______________________________________________________________________________________________________________
//

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY
{
	return NO;
}


//_______________________________________________________________________________________________________________
//

- (SGTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy
{
	return nil;
}


//_______________________________________________________________________________________________________________
//

- (SGSolutionValue)checkPuzzleSolution:(NSArray *)puzzleTiles
{
	return SGSolutionIcy;
}


@end
