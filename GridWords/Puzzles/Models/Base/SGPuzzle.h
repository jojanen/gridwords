//
//  SGPuzzle.h
//  
//
//  Created by John Ojanen on 1/11/14.
//
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, SGSolutionValue) {
	SGSolutionIcy,
	SGSolutionColder,
	SGSolutionCold,
	SGSolutionWarm,
	SGSolutionWarmer,
	SGSolutionHot,
	SGSolutionGotIt
};

//_______________________________________________________________________________________________________________
// The tile grid model

@class SGTileView;

//_______________________________________________________________________________________________________________
// 

@interface SGPuzzle : NSObject

@property (readonly, nonatomic) NSUInteger rowCount;
@property (readonly, nonatomic) NSUInteger colCount;

- (id)initWithRows:(NSUInteger)rowCount andColumns:(NSUInteger)colCount;
- (void)generateLevel:(NSInteger)level;
- (void)scramble;

- (BOOL)hasEntryAtX:(NSUInteger)iX Y:(NSUInteger)iY;
- (SGTileView *)createTileForX:(NSUInteger)ix Y:(NSUInteger)iy;

- (SGSolutionValue)checkPuzzleSolution:(NSArray *)puzzleTiles;

@end
