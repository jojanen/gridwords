//
//  DWGridView.h
//  Scramble
//
//  Created by John Ojanen on 10/13/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGLabelTileView.h"
#import "SGGridPosition.h"
#import "SGPuzzle.h"

@class SGGridView;


@protocol SGGridViewDelegate <NSObject>
@required
-(SGLabelTileView *)gridView:(SGGridView *)gridView tileForPosition:(SGGridPosition)position;
@optional
-(BOOL)gridView:(SGGridView *)gridView shouldScrollCell:(SGLabelTileView *)cell atPosition:(SGGridPosition)position;
-(void)gridView:(SGGridView *)gridView willMoveCell:(SGLabelTileView *)cell fromPosition:(SGGridPosition)fromPosition toPosition:(SGGridPosition)toPosition;
-(SGSolutionValue)gridView:(SGGridView *)gridView didMoveCell:(SGLabelTileView *)cell fromPosition:(SGGridPosition)fromPosition toPosition:(SGGridPosition)toPosition;
-(void)gridView:(SGGridView *)gridView didSelectCell:(SGLabelTileView *)cell atPosition:(SGGridPosition)position;
@end


@protocol SGGridViewDataSource <NSObject>
@required
-(NSInteger)numberOfRowsInGridView:(SGGridView *)gridView;
-(NSInteger)numberOfColumnsInGridView:(SGGridView *)gridView;
@optional
-(NSInteger)numberOfVisibleRowsInGridView:(SGGridView *)gridView;
-(NSInteger)numberOfVisibleColumnsInGridView:(SGGridView *)gridView;
@end


@interface SGGridView : UIView <UIGestureRecognizerDelegate, UIScrollViewDelegate>

@property (nonatomic,assign) id<SGGridViewDataSource>dataSource;
@property (nonatomic,assign) id<SGGridViewDelegate>delegate;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *visibleCells;

-(void)reloadData;
-(SGGridPosition)normalizePosition:(SGGridPosition)position;
@end

