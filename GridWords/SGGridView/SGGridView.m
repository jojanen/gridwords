//
//  DWGridView.m
//  Scramble
//
//  Created by John Ojanen on 10/13/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGGridView.h"
#import "SGGridPosition.h"


@interface SGGridView ( )
{
	NSInteger _numberOfRowsInGrid;
	NSInteger _numberOfColumnsInGrid;
	
	NSInteger _numberOfVisibleRowsInGrid;
	NSInteger _numberOfVisibleColumnsInGrid;
	NSInteger _outerOffset;
	
	BOOL _isMovingVertically;
	BOOL _isMovingHorizontally;
	
	SGGridPosition _lastTouchedPosition;
	CGSize _cellSize;
	CGPoint _lastScrollOffset;
}

-(void)initCells;

-(NSInteger)tagForPosition:(SGGridPosition)position;

@end


//_______________________________________________________________________________________________________________
//

@implementation SGGridView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        //set touch position to something non-existant
        _lastTouchedPosition = SGGridPositionMake(-1337, -1337);
        
        //self can't have tag 0 because there is a tile with tag 0 which will conflict when moving
        self.tag = 1337;

		UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureDetected:)];
        [self addGestureRecognizer:tapRecognizer];
		
		_outerOffset = 1;
		
		self.clipsToBounds = YES;
		self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}


//_______________________________________________________________________________________________________________
//

-(void)layoutSubviews
{
    [super layoutSubviews];
}


//_______________________________________________________________________________________________________________
//

-(void)reloadData
{
    //fetch total grid size
    _numberOfRowsInGrid = [self.dataSource numberOfRowsInGridView:self];
    _numberOfColumnsInGrid = [self.dataSource numberOfColumnsInGridView:self];
    
    //fetch the visible grid size
    if ([self.dataSource respondsToSelector:@selector(numberOfVisibleRowsInGridView:)])
        _numberOfVisibleRowsInGrid = [self.dataSource numberOfVisibleRowsInGridView:self];
    else
        _numberOfVisibleRowsInGrid = _numberOfRowsInGrid;
    
    if ([self.dataSource respondsToSelector:@selector(numberOfVisibleColumnsInGridView:)])
        _numberOfVisibleColumnsInGrid = [self.dataSource numberOfVisibleColumnsInGridView:self];
    else
        _numberOfVisibleColumnsInGrid = _numberOfColumnsInGrid;
    
    [self initCells];
}


//_______________________________________________________________________________________________________________
//

- (void)initCells
{
    //remove all subviews
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
    //fetch the bounds, will be used to position the cells
    CGRect myFrame = self.bounds;
	_cellSize.width = myFrame.size.width / _numberOfVisibleColumnsInGrid;
	_cellSize.height = myFrame.size.height / _numberOfVisibleRowsInGrid;

	//loop through the rows with 1 above and 1 below the screen
    for (NSInteger row = -_outerOffset; row < _numberOfVisibleRowsInGrid+_outerOffset; row++)
    {
        //loop through the columns with 2 left and 2 right of the screen
        for (NSInteger column = -_outerOffset; column < _numberOfVisibleColumnsInGrid+_outerOffset; column++)
        {
            //Skip items that can't be reached
            if ([self shouldSkipItemAtRow:row column:column])
            {
                continue;
            }

            SGGridPosition cellPosition = SGGridPositionMake(row, column);
            SGLabelTileView *cell = [self.delegate gridView:self tileForPosition:cellPosition];
			
			//create the frame based on the current position, the view's bounds and the grid's size
			CGRect cellFrame;
			cellFrame.size.width = _cellSize.width;
			cellFrame.size.height = _cellSize.height;
			cellFrame.origin.x = [self calcCellXforCol:column];
			cellFrame.origin.y = [self calcCellYforRow:row];
			
			cell.frame = cellFrame;
			cell.tag = [self tagForPosition:SGGridPositionMake(row, column)];

			//add the cell to the grid view
            if (![self.subviews containsObject:cell])
            {
                [self addSubview:cell];
            }
        }
    }
}


//_______________________________________________________________________________________________________________
//

-(BOOL)shouldSkipItemAtRow:(NSInteger)row column:(NSInteger)column
{
	// skip corners of outside rows and columns.
    BOOL skip = NO;
    if (row < 0 && column < 0)
    {
        skip = YES;
    }
    
    if (row < 0 && column >= _numberOfVisibleColumnsInGrid)
    {
        skip = YES;
    }
    
    if (row >= _numberOfVisibleRowsInGrid && column < 0)
    {
        skip = YES;
    }
    
    if (row >= _numberOfVisibleRowsInGrid && column >= _numberOfVisibleColumnsInGrid)
    {
        skip = YES;
    }
    
    return skip;
}


//_______________________________________________________________________________________________________________
//

- (NSInteger)tagForPosition:(SGGridPosition)position
{
    NSInteger tag = (position.row) * (_numberOfColumnsInGrid + 2) + (position.column) + 42;
    return tag;
}


//_______________________________________________________________________________________________________________
//

-(SGGridPosition)positionAtPoint:(CGPoint)point
{
    SGGridPosition position;
    CGFloat height = self.bounds.size.height;
    CGFloat posY = point.y;
    CGFloat rowHeight = height / _numberOfVisibleRowsInGrid;
    position.row = floor(posY / rowHeight);
    
    CGFloat width = self.bounds.size.width;
    CGFloat posX = point.x;
    CGFloat columnWidth = width / _numberOfVisibleColumnsInGrid;
    position.column = floor(posX / columnWidth);
    
    return position;
}


//_______________________________________________________________________________________________________________
//

-(SGGridPosition)normalizePosition:(SGGridPosition)position
{
    
    if (position.row < 0)
    {
        position.row += _numberOfRowsInGrid;
    }
	else if (position.row >= _numberOfRowsInGrid)
    {
        position.row -= _numberOfRowsInGrid;
    }
    
    if (position.column < 0)
    {
        position.column += _numberOfColumnsInGrid;
    }
	else if (position.column >= _numberOfColumnsInGrid)
    {
        position.column -= _numberOfColumnsInGrid;
    }
    
    return position;
}


//_______________________________________________________________________________________________________________
//_______________________________________________________________________________________________________________
//_______________________________________________________________________________________________________________
//	UIScrollViewDelegate callbacks

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	CGPoint touchPoint = [scrollView.panGestureRecognizer locationInView:self];
	_lastTouchedPosition = [self positionAtPoint:touchPoint];
	_lastScrollOffset = scrollView.contentOffset;
	_isMovingHorizontally = NO;
	_isMovingVertically = NO;
}


//_______________________________________________________________________________________________________________
//

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	SGGridPosition touchPosition = _lastTouchedPosition;
	CGPoint currentOffset = scrollView.contentOffset;
	CGPoint translation = CGPointMake(_lastScrollOffset.x - currentOffset.x, _lastScrollOffset.y - currentOffset.y);

	if (!_isMovingHorizontally && !_isMovingVertically)
	{
		if (fabs(translation.x) >= fabs(translation.y))
		{
			_isMovingHorizontally = YES;
		}
		else if (fabs(translation.x) < fabs(translation.y))
		{
			_isMovingVertically = YES;
		}
	}
	
	_lastScrollOffset = currentOffset;
	
	// Reset offset of scrollview if at edge.
	CGFloat leftThreshold = (_numberOfVisibleColumnsInGrid * _cellSize.width);
	CGFloat rightThreshold = scrollView.contentSize.width - (_numberOfVisibleColumnsInGrid * _cellSize.width);
	CGFloat topThreshold = (_numberOfVisibleRowsInGrid * _cellSize.height);
	CGFloat bottomThreshold = scrollView.contentSize.height - (_numberOfVisibleRowsInGrid * _cellSize.height);
	CGPoint newOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y);

	if (_isMovingHorizontally)
	{
		if (currentOffset.x < leftThreshold)
		{
			newOffset.x = scrollView.contentSize.width/2.0;
			_lastScrollOffset = newOffset;
			scrollView.contentOffset = newOffset;
		}
		else if (currentOffset.x > rightThreshold)
		{
			newOffset.x = scrollView.contentSize.width/2.0;
			_lastScrollOffset = newOffset;
			scrollView.contentOffset = newOffset;
		}
	}
	else if (_isMovingVertically)
	{
		if (currentOffset.y < topThreshold)
		{
			newOffset.y = scrollView.contentSize.height/2.0;
			_lastScrollOffset = newOffset;
			scrollView.contentOffset = newOffset;
		}
		else if (currentOffset.y > bottomThreshold)
		{
			newOffset.y = scrollView.contentSize.height/2.0;
			_lastScrollOffset = newOffset;
			scrollView.contentOffset = newOffset;
		}
	}

//	NSLog(@"contentOffset: (%f, %f)\n", scrollView.contentOffset.x, scrollView.contentOffset.y);
//	NSLog(@"tanslation: (%f, %f)\n", translation.x, translation.y);
	if (_isMovingHorizontally)
		[self moveCellHorizontallyAtPosition:touchPosition withTranslation:translation];
	else if (_isMovingVertically)
		[self moveCellVerticallyAtPosition:touchPosition withTranslation:translation];
	
}


//_______________________________________________________________________________________________________________
//

// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
	/**
	 * Here we target a specific cell index to move towards
	 */
	if (_isMovingHorizontally)
	{
		CGFloat newIndex_f = targetContentOffset->x / _cellSize.width;
		NSInteger nextIndex;
		nextIndex = roundf(newIndex_f);
//		if (velocity.x == 0.0f)
//		{
//			// A 0 velocity means the user dragged and stopped (no flick)
//			// In this case, tell the scroll view to animate to the closest index
//			nextIndex = roundf(newIndex_f);
//		}
//		else if (velocity.x > 0.0f)
//		{
//			// User scrolled right
//			// Evaluate to the nearest index
//			nextIndex = floor(newIndex_f);
//		}
//		else
//		{
//			// User scrolled left
//			// Evaluate to the nearest index
//			nextIndex = ceil(newIndex_f);
//		}
		// Return our adjusted target point
		targetContentOffset->x = nextIndex * _cellSize.width;
	}
	else if (_isMovingVertically)
	{
		CGFloat newIndex_f = targetContentOffset->y / _cellSize.height;
		NSInteger nextIndex;
		nextIndex = roundf(newIndex_f);
//		if (velocity.y == 0.0f)
//		{
//			// A 0 velocity means the user dragged and stopped (no flick)
//			// In this case, tell the scroll view to animate to the closest index
//			nextIndex = roundf(newIndex_f);
//		}
//		else if (velocity.y > 0.0f)
//		{
//			// User scrolled downwards
//			// Evaluate to the nearest index
//			nextIndex = floor(newIndex_f);
//		}
//		else
//		{
//			// User scrolled upwards
//			// Evaluate to the nearest index
//			nextIndex = ceil(newIndex_f);
//		}
		// Return our adjusted target point
		targetContentOffset->y = nextIndex * _cellSize.height;
	}
}


//_______________________________________________________________________________________________________________
//

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (decelerate == NO)
	{
		_isMovingHorizontally = NO;
		_isMovingVertically = NO;
		[self reloadData];
	}
}


//_______________________________________________________________________________________________________________
//

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	_isMovingHorizontally = NO;
	_isMovingVertically = NO;
	[self reloadData];
}


//_______________________________________________________________________________________________________________
//_______________________________________________________________________________________________________________
//_______________________________________________________________________________________________________________
//

-(void)tapGestureDetected:(UITapGestureRecognizer *)gesture
{
    if (_lastTouchedPosition.row >= 0 && _lastTouchedPosition.column >= 0)
    {
//        [UIView animateWithDuration:0.2 animations:^
//        	{
//            	[self reloadData];
//        	}
//			completion:^(BOOL finished)
//         	{
//             	_lastTouchedPosition = SGGridPositionMake(-55, -55);
//         	}];
    }
    
    if ([self.delegate respondsToSelector:@selector(gridView:didSelectCell:atPosition:)])
    {
        SGGridPosition touchPosition = [self positionAtPoint:[gesture locationInView:self]];
        if (touchPosition.row != _lastTouchedPosition.row && touchPosition.column != _lastTouchedPosition.column)
        {
            SGLabelTileView *cell = [self.delegate gridView:self tileForPosition:touchPosition];
            [self.delegate gridView:self didSelectCell:cell atPosition:touchPosition];
        }
    }
    
}


//_______________________________________________________________________________________________________________
//

#pragma mark - movement

-(void)moveCellHorizontallyAtPosition:(SGGridPosition)position withTranslation:(CGPoint)translation
{
    for (NSInteger i = -_outerOffset; i< _numberOfVisibleColumnsInGrid+_outerOffset; i++)
    {
        UIView *cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(position.row, i)]];
        
        CGPoint center = cell.center;
        center.x += translation.x;
        cell.center = center;
    }
    
    UIView *cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(position.row, 0)]];
	CGFloat columnWidth = _cellSize.width;
    CGFloat posX = cell.frame.origin.x;
    if (posX >= columnWidth - 5.0)
    {
        /*SGSolutionValue val = */[self.delegate gridView:self didMoveCell:[self.delegate gridView:self tileForPosition:position] fromPosition:position toPosition:SGGridPositionMake(position.row, position.column +1)];
//		NSLog(@"Solution val = %lu\n", (long)val);
		[self refreshCellRow:position.row xOffset:posX - columnWidth];
  }
    else if (posX <= (-columnWidth + 5.0))
    {
        /*SGSolutionValue val = */[self.delegate gridView:self didMoveCell:[self.delegate gridView:self tileForPosition:position] fromPosition:position toPosition:SGGridPositionMake(position.row, position.column -1)];
//		NSLog(@"Solution val = %lu\n", (long)val);
		[self refreshCellRow:position.row xOffset:posX - (-columnWidth)];

    }
}


//_______________________________________________________________________________________________________________
//

-(void)moveCellVerticallyAtPosition:(SGGridPosition)position withTranslation:(CGPoint)translation
{
    for (NSInteger i = -_outerOffset; i < _numberOfVisibleRowsInGrid+_outerOffset; i++)
    {
        UIView *cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(i, position.column)]];
        CGPoint center = cell.center;
        center.y += translation.y;
        cell.center = center;
    }
    
    UIView *cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(0, position.column)]];
	CGFloat rowHeight = _cellSize.height;
    CGFloat posY = cell.frame.origin.y;
    if (posY >= rowHeight - 5.0/* && posY < rowHeight + 3.0*/)
    {
        /*SGSolutionValue val = */[self.delegate gridView:self didMoveCell:[self.delegate gridView:self tileForPosition:position] fromPosition:position toPosition:SGGridPositionMake(position.row+1, position.column)];
//		NSLog(@"Solution val = %lu\n", (long)val);
		[self refreshCellColumn:position.column yOffset:posY - rowHeight];
    }
	else if (posY <= (-rowHeight + 5.0)/* && posY > (-rowHeight + 3.0)*/)
    {
        /*SGSolutionValue val = */[self.delegate gridView:self didMoveCell:[self.delegate gridView:self tileForPosition:position] fromPosition:position toPosition:SGGridPositionMake(position.row-1, position.column)];
//		NSLog(@"Solution val = %lu\n", (long)val);
		[self refreshCellColumn:position.column yOffset:posY - (-rowHeight)];
    }
}


//_______________________________________________________________________________________________________________
//

- (void)refreshCellRow:(NSInteger)refreshRow xOffset:(CGFloat)xOffset
{
	UIView *cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(refreshRow, -_outerOffset)]];
	[cell performSelector:@selector(removeFromSuperview)];

	cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(refreshRow, _numberOfVisibleColumnsInGrid)]];
	[cell performSelector:@selector(removeFromSuperview)];

	CGFloat rowY = [self calcCellYforRow:refreshRow];
	
	//loop through the columns
	for (NSInteger column = -_outerOffset; column < _numberOfVisibleColumnsInGrid+_outerOffset; column++)
	{
		SGGridPosition cellPosition = SGGridPositionMake(refreshRow, column);
		SGLabelTileView *cell = [self.delegate gridView:self tileForPosition:cellPosition];
		
		//create the frame based on the current position, the view's bounds and the grid's size
		CGRect cellFrame;
		cellFrame.size.width = _cellSize.width;
		cellFrame.size.height = _cellSize.height;
		cellFrame.origin.x = [self calcCellXforCol:column] + xOffset;
		cellFrame.origin.y = rowY;
		cell.frame = cellFrame;
		cell.tag = [self tagForPosition:SGGridPositionMake(refreshRow, column)];

		//add the cell to the grid view if missing
		if (![self.subviews containsObject:cell])
		{
			[self addSubview:cell];
		}
	}
}


//_______________________________________________________________________________________________________________
//

- (void)refreshCellColumn:(NSInteger)refreshCol yOffset:(CGFloat)yOffset
{
	UIView *cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(-_outerOffset, refreshCol)]];
	[cell performSelector:@selector(removeFromSuperview)];
	
	cell = [self viewWithTag:[self tagForPosition:SGGridPositionMake(_numberOfVisibleRowsInGrid, refreshCol)]];
	[cell performSelector:@selector(removeFromSuperview)];
	
	CGFloat rowX = [self calcCellXforCol:refreshCol];
	
	//loop through the rows
	for (NSInteger row = -_outerOffset; row < _numberOfVisibleColumnsInGrid+_outerOffset; row++)
	{
		SGGridPosition cellPosition = SGGridPositionMake(row, refreshCol);
		SGLabelTileView *cell = [self.delegate gridView:self tileForPosition:cellPosition];
		
		//create the frame based on the current position, the view's bounds and the grid's size
		CGRect cellFrame;
		cellFrame.size.width = _cellSize.width;
		cellFrame.size.height = _cellSize.height;
		cellFrame.origin.x = rowX;
		cellFrame.origin.y = [self calcCellYforRow:row] + yOffset;
		cell.frame = cellFrame;
		cell.tag = [self tagForPosition:SGGridPositionMake(row, refreshCol)];
		
		//add the cell to the grid view if missing
		if (![self.subviews containsObject:cell])
		{
			[self addSubview:cell];
		}
	}
}


//_______________________________________________________________________________________________________________
//

-(NSArray *)visibleCells
{
    NSMutableArray *visibleCells = [[NSMutableArray alloc] init];
    for (UIView *v in self.subviews)
    {
        if ([v isKindOfClass:[SGLabelTileView class]] && CGRectIntersectsRect(v.frame, self.bounds))
        {
            [visibleCells addObject:v];
        }
    }
    return [NSArray arrayWithArray:visibleCells];
}


//_______________________________________________________________________________________________________________
// MARK: Utilities

- (CGFloat)calcCellXforCol:(NSInteger)column
{
	CGFloat xVal = column * _cellSize.width;
	return xVal;
}


//_______________________________________________________________________________________________________________
//

- (CGFloat)calcCellYforRow:(NSInteger)row
{
	CGFloat yVal = row * _cellSize.height;
	return yVal;
}


@end