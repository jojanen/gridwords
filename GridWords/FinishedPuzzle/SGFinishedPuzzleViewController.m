//
//  SGFinishedPuzzleViewController.m
//  Scramble
//
//  Created by John Ojanen on 12/2/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import "SGFinishedPuzzleViewController.h"
#import "UIFont+SystemOverride.h"

@interface SGFinishedPuzzleViewController ()

@property (weak, nonatomic) IBOutlet UIButton *replayButton;
@property (weak, nonatomic) IBOutlet UIButton *nwGridButton;
@property (weak, nonatomic) IBOutlet UITextView *foundWordsTextView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

- (IBAction)handleReplay:(id)sender;
- (IBAction)handleNext:(id)sender;
- (IBAction)handleMenu:(id)sender;

@end

@implementation SGFinishedPuzzleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)handleReplay:(id)sender
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(replayPuzzleChosen)])
		[self.delegate replayPuzzleChosen];
}

- (IBAction)handleNext:(id)sender
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(nextPuzzleChosen)])
		[self.delegate nextPuzzleChosen];
}

- (IBAction)handleMenu:(id)sender
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(mainMenuChosen)])
		[self.delegate mainMenuChosen];
}


//_______________________________________________________________________________________________________________
//

- (void)setFoundWords:(NSString *)foundWords
{
	self.foundWordsTextView.text = foundWords;
}


- (void)setScore:(NSInteger)score
{
	self.scoreLabel.text = [[NSNumber numberWithInteger:score] stringValue];
}


@end
