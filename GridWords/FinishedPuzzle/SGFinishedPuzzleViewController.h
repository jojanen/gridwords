//
//  SGFinishedPuzzleViewController.h
//  Scramble
//
//  Created by John Ojanen on 12/2/14.
//  Copyright (c) 2014 John Ojanen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SGFinishedScreenDelegate <NSObject>

- (void)replayPuzzleChosen;
- (void)nextPuzzleChosen;
- (void)mainMenuChosen;

@end


@interface SGFinishedPuzzleViewController : UIViewController

@property (nonatomic, weak) id<SGFinishedScreenDelegate> delegate;

- (void)setFoundWords:(NSString *)foundWords;
- (void)setScore:(NSInteger)score;

@end
